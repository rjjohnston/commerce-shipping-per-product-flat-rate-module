<?php

namespace Drupal\commerce_shipping_per_product\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Provides the Per-product Flat Rate shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "shipping_per_product_flat_rate",
 *   label = @Translation("Per-product flat rate"),
 * )
 */
class PerProductFlatRate extends ShippingMethodBase implements ContainerFactoryPluginInterface
{

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
    protected $entityTypeManager;

    /**
     * Constructs a new PerProductFlatRate object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
     *   The package type manager.
     * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
     *   The workflow manager.
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

        $this->services['default'] = new ShippingService('default', $this->configuration['rate_label']);
        //Removed from __construct() EntityTypeManagerInterface $entity_type_manager
        $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow')
//      $container->get('entity_type.manager')
    );
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
      'rate_label' => '',
      'default_rate_amount' => '',
      'rate_amount' => null,
      'services' => ['default'],
    ] + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form, $form_state);

        $amount = $this->configuration['default_rate_amount'];
        // A bug in the plugin_select form element causes $amount to be incomplete.
        if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
            $amount = null;
        }

        $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => t('Rate label'),
      '#label' => t('Rate label'), //test
      '#description' => t('Shown to customers during checkout.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => true,
    ];

        $form['default_rate_amount'] = [
      '#type' => 'commerce_price',
      '#title' => t('Default rate amount'),
      '#label' => t('Default rate amount'), //test
      '#description' => t('Per-item rate used when neither a per-variation flat rate nor a per-product flat rate is defined.'),
      '#default_value' => $amount,
      '#required' => true,
    ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);

        if (!$form_state->getErrors()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['rate_label'] = $values['rate_label'];
            $this->configuration['default_rate_amount'] = $values['default_rate_amount'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function calculateRates(ShipmentInterface $shipment)
    {
        // Rate IDs aren't used in a flat rate scenario because there's always a
        // single rate per plugin, and there's no support for purchasing rates.

        // Default rate when neither per-variation nor per-product rates have been set.
        $default_rate = $this->configuration['default_rate_amount'];
        $currency_code = $default_rate['currency_code'];
        $total_rate = new Price(0, $default_rate['currency_code']);
        $default_rate = new Price($default_rate['number'], $default_rate['currency_code']);

        $order_item_storage = $this->entityTypeManager->getStorage('commerce_order_item');
        foreach ($shipment->getItems() as $shipment_item) {
            $rate = null;
            $addonrate = null;
            $add_item_rate = null;
            $orderItem = $order_item_storage->load($shipment_item->getOrderItemId());

            // Attempt to get per-variation flat rate.
            $variation = $orderItem->getPurchasedEntity();
            // Load bundle name for conditional down when running quanitiy custom code
            $variation_bundle = $variation->getProduct()->bundle();
            //\Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("variation bundle " . json_encode($variation_bundle));
            if ($variation->hasField('shipping_flat_rate')) {
                if (!$variation->get('shipping_flat_rate')->isEmpty()) {
                    $rate = $variation->get('shipping_flat_rate')->first()->toPrice();
                }
            }

            // If no per-variation flat rate, attempt to get per-product flat rate.
            if (!isset($rate)) {
                $product = $variation->getProduct();
                if ($product->hasField('shipping_flat_rate')) {
                    if (!$product->get('shipping_flat_rate')->isEmpty()) {
                        $rate = $product->get('shipping_flat_rate')->first()->toPrice();
                    }
                }
            }
            // check for additional item shipping field
            if ($variation->hasField('field_additional_item_shipping')) {
                if (!$variation->get('field_additional_item_shipping')->isEmpty()) {
                    $add_item_rate = $variation->get('field_additional_item_shipping')->first()->toPrice();
                }
            }

            // Fallback to the plugin default flat rate.
            if (!isset($rate)) {
                $rate = $default_rate;
            }
            $quantity = $shipment_item->getQuantity();

            // 02-29-2020 - RJ Added 'prep' condition so we don't only charge $1 for each additional item
            // 08142020 - RJ Updating and fixing additional rate code to properly charge the additional rate of ALL additional items
            // 09122020 - RJ Fix additional rate calc

            if ($quantity > 1) { 
                \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("Quantity" . $quantity);
                // Check for additional rate field
                if (!empty($add_item_rate)) 
                {
                    $r = $rate->toArray();
                    // \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("base rate" . json_encode($r));
                    $ai = $add_item_rate->toArray();
                    // \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("additional item rate" . json_encode($ai));
                    $reduced_quantity = $quantity - 1;
                    $additional_cost = $add_item_rate->multiply((string) $reduced_quantity);//$rate->add($add_item_rate);
                    $rate = $rate->add($additional_cost);
                } else {
                    // No additional rate field present, add the standard $1 per item
                    $quantity--;
                    $addonrate = new Price(1, $currency_code);
                    $addonrate = $addonrate->multiply((string) $quantity);
                    // Debug
                    $p = $addonrate->toArray();
                    $r = $rate->toArray();
                    // \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("base rate" . json_encode($r));
                    // \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("add on rate" . json_encode($p));
              
                    $rate = $rate->add($addonrate);
                }

                //Debug
                $t = $rate->toArray();
                // \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("total rate" . json_encode($t));

            } else {
                // Debug
                //$q = $shipment_item->getQuantity();
                //\Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("prep quantity " . json_encode($q));
          
                $rate = $rate->multiply((string) $shipment_item->getQuantity());
            }

            $total_rate = $total_rate->add($rate);
            // Debug
            $f = $total_rate->toArray();
            // \Drupal::logger('commerce_shipping_per_product_flat_rate')->notice("final rate" . json_encode($f));
        }

        $rate_id = 3;
        $rates = [];
        $rates[] = new ShippingRate([
          'shipping_method_id' => $rate_id,
          'service' => $this->services['default'],
          'amount' => $total_rate,
        ]);

        return $rates;
    }
}
