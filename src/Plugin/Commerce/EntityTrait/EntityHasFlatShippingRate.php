<?php

namespace Drupal\commerce_shipping_per_product\Plugin\Commerce\EntityTrait;

use Drupal\commerce\BundleFieldDefinition;
use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;

/**
 * Provides the "purchasable_entity_shippable" trait.
 *
 * @CommerceEntityTrait(
 *   id = "entity_has_flat_shipping_rate",
 *   label = @Translation("Enable flat shipping rate"),
 *   entity_types = {"commerce_product", "commerce_product_variation"}
 * )
 */
class EntityHasFlatShippingRate extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];
    $fields['shipping_flat_rate'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel('Shipping flat rate amount')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
      'type' => 'commerce_price_default',
      'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'commerce_price_default',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}