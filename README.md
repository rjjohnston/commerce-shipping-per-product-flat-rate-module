# Commerce Shipping Per Product Flat Rate module

Provides a per-product shipping method plugin for Commerce Shipping.

## Setup

1. Install the module.

## Example

```
"repositories": {
    "commerce_shipping_per_product": {
             "type": "package",
             "package": {
                 "name": "rjjohnston/commerce_shipping_per_product",
                 "version": "master",
                 "type": "drupal-module",
                 "source": {
                     "url": "https://rjjohnston@bitbucket.org/rjjohnston/commerce-shipping-per-product-flat-rate-module.git",
                     "type": "git",
                     "reference": "origin/master"
                 }
             }
         }
 }
 ```
 Further down the composer.json file:
 
 ```
 "require": {
    "rjjohnston/commerce_shipping_per_product": "master",
 }
 ```
 
 Run composer update

2. Edit your shipping method type and enable the per-product flat rate plugin:
  - Select the default package type.
  - Select the shipping services (if more than one exists).
  - Enter the rate label to be shown to customers during checkout.
  - Enter a default rate amount, to be used when the product does not have a defined rate.

3. To define product variation-specific rates:
  - Edit the product variation type and enable the 'Enable flat shipping rate' trait.
  - Enter the 'Shipping flat rate amount' for each variation of that type.

4. To define product-specific rates:
  - Edit the product type and enable the 'Enable flat shipping rate' trait.
  - Enter the 'Shipping flat rate amount' for each product of that type.

## Shipping rate calculation

For each item in the cart, the plugin will use the rate defined for the
product variation, if it exists. If a per-variation rate is not defined for the
item, then the rate defined for the product will be used. If neither the product
variation nor the product has a defined rate, then the plugin will use the
default rate (defined for the shipping method).

Once the appropriate rate is determined for an item, that rate will be
multiplied for each quantity of the item. The total shipping rate calculated
will be the sum of the multipled rates for all items in the cart.

## To remove simply use this line

'''
composer remove rjjohnston/commerce_shipping_per_product
'''

## Originally found here:
https://github.com/lisastreeter/commerce_shipping_ppr

